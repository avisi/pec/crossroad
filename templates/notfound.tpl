<!DOCTYPE html>
<html lang="en">
{{ template "head.tpl" . }}

<body>
    <div style="margin-top:50px"></div>
    <div class="container text-center">
        <div class="row">
            <div class="col">
                <h1>Oops!</h1>
                <p class="lead">404 Not Found</p>
                <div class="error-details">
                    Sorry, the requested page could not be found!<br>
                </div>
                <div class="error-actions">
                    <a href="{{ .BaseURL }}" class="btn btn-default">Take Me Home</a>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>
