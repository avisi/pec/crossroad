FROM golang:1.12.0-alpine3.9

EXPOSE 8000
WORKDIR /go/src/app

ADD . .
RUN go build -o crossroad

CMD ["./crossroad"]
