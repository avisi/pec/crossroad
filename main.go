package main

import log "github.com/cihub/seelog"

import (
	"github.com/gorilla/mux"

	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"text/template"
	"time"

	yaml "gopkg.in/yaml.v2"
)

// TemplateProperties is a generic template properties object to pass to template rendering functions
type TemplateProperties struct {
	BaseURL     string
	Title       string
	Description string
	Config      DashboardConfig
}

var templates *template.Template

func main() {
	defer log.Flush()

	baseURL := flag.String("baseUrl", getEnv("baseUrl", "http://localhost:8000"), "Base URL for the webapplication")
	title := flag.String("title", getEnv("title", "crossroad"), "html webpage title")
	description := flag.String("description", getEnv("description", "crossroad"), "html webpage description")
	configFile := flag.String("config", getEnv("config", "dashboard.yaml"), "Path to the dashboard yaml configuration file")

	flag.Parse()
	parseTemplates()

	config, _ := getConfig(*configFile)

	r := mux.NewRouter()
	r.HandleFunc("/", func(res http.ResponseWriter, req *http.Request) {
		parseTemplates()

		render(res, "index.tpl", TemplateProperties{
			BaseURL:     *baseURL,
			Title:       *title,
			Description: *description,
			Config:      *config,
		})
	})
	r.PathPrefix("/static").Handler(noDirListing(http.StripPrefix("/static", http.FileServer(http.Dir("public")))))
	r.PathPrefix("/").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		parseTemplates()

		w.WriteHeader(404)
		render(w, "notfound.tpl", TemplateProperties{
			BaseURL:     *baseURL,
			Title:       *title,
			Description: *description,
			Config:      *config,
		})
	})

	srv := &http.Server{
		Handler: r,
		Addr:    "0.0.0.0:8000",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Info("Starting web server...")

	srv.ListenAndServe()
}

func parseTemplates() {
	var allFiles []string
	files, err := ioutil.ReadDir("./templates")
	if err != nil {
		fmt.Println(err)
	}
	for _, file := range files {
		filename := file.Name()
		if strings.HasSuffix(filename, ".tpl") {
			allFiles = append(allFiles, "./templates/"+filename)
		}
	}

	templates, err = template.ParseFiles(allFiles...)
	if err != nil {
		fmt.Println(err)
	}
}

func noDirListing(h http.Handler) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.HasSuffix(r.URL.Path, "/") {
			http.NotFound(w, r)
			return
		}
		h.ServeHTTP(w, r)
	})
}

func render(response http.ResponseWriter, templateName string, data interface{}) {
	t := templates.Lookup(templateName)
	t.Execute(response, data)
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func getConfig(path string) (*DashboardConfig, error) {
	configData, readFileError := ioutil.ReadFile(path)
	if readFileError != nil {
		return nil, readFileError
	}
	configuration := &DashboardConfig{}
	readConfigError := yaml.Unmarshal([]byte(configData), &configuration)
	if readConfigError != nil {
		return nil, readConfigError
	}
	return configuration, nil
}

type DashboardConfig struct {
	Urls []DashboardURL `json:"urls"`
}

type DashboardURL struct {
	Title string `json:"title"`
	URL   string `json:"url"`
}
