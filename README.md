# Crossroad

[![build status](https://gitlab.com/avisi/pec/crossroad/badges/master/build.svg)](https://gitlab.com/avisi/pec/crossroad/commits/master)

Handles static pages to display a bunch of buttons leading to urls for management tooling.

## Contributing

- Want to report a bug or request a feature? Please open [an issue](https://gitlab.com/avisi/pec/crossroad/issues).
- Want to help us with bug fixes or new web pages? We are happy with all the help we can get!

Everyone is welcome to contribute to this project. You can help out with the ongoing development by looking for potential bugs in our code base, or by contributing new features. To contribute something to this project, simply fork this repository and submit your pull requests for review by other collaborators.

### Building

Building the project:

```bash
$ go build
```

This will produce a new binary executable file that can be run. If you are on windows, it will be a `.exe` file.

#### Using Docker

Alternatively, you can use `docker build -t avisi/pec/crossroad .` to create a Docker image for this project. This will build the project for you and allows you to start and run it.

There is also a `docker-compose.yml` file to easily get started with this. Simply run `docker-compose up -d` to build and start the project. No `golang` or `dep` installations are required, just Docker and docker-compose.

To rebuild the site, run `docker-compose build` and then rerun `docker-compose up -d`.

### New dependencies

When adding new dependencies, install them through `dep`. You can do so by issueing the following command: `dep ensure -add <package>`. Example:
```bash
dep ensure -add github.com/gorilla/handlers
```

This will add the new package to Gopkg.yaml and Gopkg.lock, as well as download the dependency and add it to the vendor directory. Please do not forget to also check in all files in the vendor directory.
